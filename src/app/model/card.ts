export interface Card {
  title: string;
  image: string;
  message: string;
  footer: string;
}
