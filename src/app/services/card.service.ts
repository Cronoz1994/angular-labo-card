import {Injectable} from '@angular/core';
import {Card} from '../model/card';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  private _cards: Card [];

  constructor() {
    this._cards = [];
  }

  public fillList(numberCards: number) {
    for (let i = 0; i < numberCards; i++) {
      const card: Card = {
        title: `Title ${i + 1}`,
        image: '/assets/images/card-image.jpg',
        message: `dkmask mdklasm kldmlas dlskam dklas dma;smdaskmdklsamdkmsakldm
                  sla dmasdm slamdksalkdmlasmdlmsakldmsa m dksam dlas mdlkasm kld
                  maslkm dskla d mamdlskam kdlmaklmdkl`,
        footer: `Footer ${i + 1}`
      };

      this._cards.push(card);
    }
  }

  get cards(): Card [] {
    return this._cards;
  }

}
