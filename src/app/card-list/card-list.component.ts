import {Component, OnInit} from '@angular/core';

import {CardService} from '../services/card.service';

import {Card} from '../model/card';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {
  public cards: Card [];

  constructor(private cardService: CardService) {
    this.cards = [];
  }

  ngOnInit() {
    this._initialize();
  }

  public _initialize(): void {
    this.cardService.fillList(15);
    this.cards = this.cardService.cards;
  }

}
